const Hapi = require('hapi');
const Inert = require('inert');
const Vision = require('vision');
const Joi = require('joi');
const HapiSwagger = require('hapi-swagger');
const Html2png = require('html2png');

const server = new Hapi.Server();
server.connection({
    host: 'localhost',
    port: 3000
});

const options = {
    info: {
        'title': 'Test API Documentation',
        'version': '0.0.1',
    }
};

server.register([
    Inert,
    Vision,
    {
        'register': HapiSwagger,
        'options': options
    }], (err) => {
        server.start((err) => {
            if (err) {
                console.log(err);
            } else {
                console.log('Server running at:', server.info.uri);
            }

            // genfileImage
            server.route({
                path: '/api/genFileImage',
                method: 'POST',
                config: {
                    handler: (request, reply) => {
                        var html2png = Html2png;
                        var entity = {
                            schoolName: request.payload.schoolName,
                            nickName: request.payload.nickName,
                            fullName: request.payload.fullName,
                            imagePhoto: request.payload.imagePhoto,
                            backgroundImage: request.payload.bkImageBase64
                        }
                        html2png.createFileImage(entity).then(success=>{
                            reply(success);
                        }).catch((error=>{
                            reply(error);
                        }));

                        
                    },
                    description: 'Get algebraic sum',
                    notes: 'Pass two numbers as a & b and returns sum',
                    tags: ['api'],
                    validate: {
                        payload: {
                            schoolName: Joi.string().required(),
                            nickName: Joi.string().required(),
                            fullName: Joi.string().required(),
                            imagePhoto: Joi.string().required(),
                            bkImageBase64: Joi.string().required()
                        }
                    }
                }
            });

            // genBase64
            server.route({
                path: '/api/genFileImageBase64',
                method: 'POST',
                config: {
                    handler: (request, reply) => {
                        var html2png = Html2png;
                        var entity = {
                            schoolName: request.payload.schoolName,
                            nickName: request.payload.nickName,
                            fullName: request.payload.fullName,
                            imagePhoto: request.payload.imagePhoto,
                            backgroundImage: request.payload.bkImageBase64
                            
                        }
                        html2png.createFileImageBase64(entity).then(success=>{
                            reply(success);
                        }).catch((error=>{
                            reply(error);
                        }));
                        
                    },
                    description: 'Get algebraic sum',
                    notes: 'Pass two numbers as a & b and returns sum',
                    tags: ['api'],
                    validate: {
                        payload: {
                            schoolName: Joi.string().required(),
                            nickName: Joi.string().required(),
                            fullName: Joi.string().required(),
                            imagePhoto: Joi.string().required(),
                            bkImageBase64: Joi.string().required()
                        }
                    }
                }
            });

        });
});